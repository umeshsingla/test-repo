// column names
var Column = require("cloud/Column.js");

// models
var Rides = require("cloud/models/rides.js");
var Availability = require("cloud/models/availability.js");
var Route = require("cloud/models/Route.js");
var RouteCombination = require("cloud/models/RouteCombination.js");
var BusStop = require("cloud/models/busStop.js");
var Vehicle = require("cloud/models/Vehicle.js");
var Driver = require("cloud/models/Driver.js");
var RideCount = require("cloud/models/RideCount");
var GeneralETA = require("cloud/models/GeneralETA");
var errors = require("cloud/errors");

// helpers
var async = require("cloud/modules/async.js");

// create rides
Parse.Cloud.define("createRide", function (request, response) {
    var newRide = new Rides();

    // set route Id
    newRide.setRouteObject(request.params.routeId);

    // set driver Details
    newRide.setDriverObject(request.params.driverId);

    // set vehicle details
    newRide.setVehicleObject(request.params.vehicleId);

    var startDateTime = new Date(request.params.startDateTime);
    newRide.setStartDateTime(startDateTime);

    // set ride state
    newRide.setState("idle");

    // busStops object for ride object
    var busStops = [];
    var driverObj = newRide.getDriverObject();
    driverObj.fetch().then(function (driver) {
        newRide.setDriverName(driver.getFullName());
        newRide.setDriverNumber(driver.getPhoneNumber());
    }).then(function () {
        var query = Vehicle.getQuery();
        query.include(Column.VEHICLE_CATEGORY_OBJECT);
        var vehicleId = newRide.getVehicleObject().id;
        return query.get(vehicleId);
    }).then(function (vehicle) {
        newRide.setLoginId(vehicle.getLoginId());
        var vehicleCategoryObj = vehicle.getCategoryObject();
        newRide.setVehicleCategory(vehicleCategoryObj.getName());
        newRide.setVehicleRegistration(vehicle.getVehicleRegistration());
        return Parse.Promise.as();
    }).then (function () {
        var routeObj = newRide.getRouteObject();
        return routeObj.fetch()
    }).then(function (currentRoute) {
        // list of busstop id's
        var busStopIds = currentRoute.getBusstopSequence();

        // push busstop id's and default fields
        for (var i = 0; i < busStopIds.length; i++) {
            busStops.push({
                id: busStopIds[i],
                arrived: false
            });
        }

        var query = RouteCombination.getQuery();

        query.equalTo(Column.ROUTE_ID, currentRoute.id);
        query.equalTo(Column.ROUTE_FROM_ID, busStopIds[0]);

        return query.find();
    }).then(function AddArrivalTimes(rcArray) {
        for (var i = 0; i < busStops.length; i++) {
            // find the route combination to get durationValue
            for (var j = 0; j < rcArray.length; j++) {

                // add duration value to startTime
                if (rcArray[j].getToId() === busStops[i].id) {
                    // arrival time stored  as Date objects
                    var startTime = startDateTime;
                    var arrivalTime = new Date(startTime.getTime() + (rcArray[j].getDurationValue() * 1000)).toString();
                    busStops[i].arrivalTime = arrivalTime;
                    break;
                }
            }
        }
        // set startTime for initial busstop
        busStops[0].arrivalTime = startDateTime.toString();
        return;
    }).then(function setBusStops() {
        newRide.setBusStops(busStops);
        return;
    }).then(function getRideCount() {
        var query = RideCount.getQuery();
        var dateString = startDateTime.toDateString();
        var date = new Date(dateString);
        
        query.equalTo(Column.RIDE_COUNT_DATE, date);
        return query.find().then(function setCount(results) {
            if (results.length > 1) {
                return Parse.Promise.error(" There should be only one entry for each date. Multiple entries are found");
            }
            if (results.length == 0) {
                var rideCount = new RideCount();
                rideCount.setDate(date);
                rideCount.setCount(2);
                return rideCount.save()
            } else {
                var rideCountObj = results[0];
                rideCountObj.increment(Column.RIDE_COUNT_COUNT, 2);
                return rideCountObj.save()
            }
        }).then (function (rideCountObj) {
            return rideCountObj;
        }, function (error) {
            return Parse.Promise.error(errors.getErrorString(error));
        })
    }).then(function saveRide(rideCountObj) {
        // console.log("rideCount : "+ rideCountObj.getCount());
        newRide.setCountIndex(rideCountObj.getCount());
        return newRide.save();
    }).then(function (result) {
        response.success(result);
    }, function (error) {
        response.error(error.message);
    });
});

Parse.Cloud.afterSave("Rides", function (request) {
    var createdAt = request.object.get("createdAt");
    var updatedAt = request.object.get("updatedAt");
    var objectExisted = (createdAt.getTime() != updatedAt.getTime());
    // TODO: Modify objectExisted to request.object.existed() after the parse bug is fixed.
    if (objectExisted) {
        // Do nothing if the user is already existing.
    } else {
        var newAvailability = new Availability();
        newAvailability.setRideId(request.object.id);
        var vehicleObject = request.object.getVehicleObject();
        vehicleObject.fetch().then(function (vehicleObj) {
            newAvailability.setTickets(vehicleObj.getCapacity(), request.object.getBusStops().length);
            return newAvailability.save();
        }, function (error) {
            console.error(error.message);
            return Parse.Promise.error(errors.getErrorString(error));
        });
    }
});

function saveGeneralETA(rideObj) {
    var generalETA = new GeneralETA();
    generalETA.setGeoLocation(rideObj.getBusLocation());
    generalETA.setRouteObject(rideObj.getRouteObject());
    generalETA.setRideObject(rideObj)
    return generalETA.save();
}


// update busLocation
Parse.Cloud.define("updateRideLocation", function (request, response) {
    var query = Rides.getQuery();

    query.get(request.params.rideId).then(function (currentRide) {
        currentRide.setBusLocation(request.params.busLocation);

        async.map(currentRide.getBusStops(), function (currentBusStop, callback) {
            // TODO: Here we are calling getDistanceAndTime once for each busStop.
            // Modify this to make a single call for all the busStops.

            var BusStopQuery = BusStop.getQuery();
            BusStopQuery.get(currentBusStop.id).then(function (busStopObj) {
                var data = {
                    from: request.params.busLocation,
                    to: busStopObj.getGeoLocation()
                };

                // return update busstop object
                return Parse.Cloud.run("getDistanceAndTime", data).then(function (result) {

                    currentBusStop.distanceText = result.distance.text;
                    currentBusStop.distanceValue = result.distance.value;

                    currentBusStop.durationText = result.duration.text;
                    currentBusStop.durationValue = result.duration.value;

                    return currentBusStop;
                }, function (error) {
                    return Parse.Promise.error(errors.getErrorString(error));
                });

            }).then(function (result) {
                // return busstop object
                callback(null, result);
            }, function (error) {
                callback(error, null);
            });

        }, function (err, results) {

            if (err) {
                console.error(err);
                response.error(err.message);
            }
            else {
                console.log("All busstops updated");
                currentRide.setBusStops(results);
                currentRide.save().then(function (ride) {
                   saveGeneralETA(currentRide).then(function (success) {
                       response.success("bus location updated");
                   }, function (error) {
                       // Send Error LOg Mail Here;
                       console.error("Saving General ETA Failed with error: " + errors.getErrorString(error));
                       // We send success here because saving ETA is not related to
                       // ride updating.
                       response.success("bus location updated");
                   })
                }, function (error) {
                    response.error("BusLocation Update Failed");
                })
            }
        });
    }, function (error) {
        console.log("rideId fetching failed: " + request.params.rideId);
        response.error(error);
    });

});

Parse.Cloud.define("getAllRides", function (request, response) {
    // Get all the rides.
    var query = Rides.getQuery();

    query.find().then(function (rides){
        response.success(rides);
    }, function (error) {
        response.error(errors.getErrorString(error));
    })
})

Parse.Cloud.define("getRidesByRouteId", function (request, response) {
    var query = Rides.getQuery();
    var routeObj = new Route();
    routeObj.id = request.params.routeId;
    query.equalTo(Column.RIDE_ROUTE_OBJ, routeObj);
    query.find().then(function (rides) {
        response.success(rides);
    }, function (error) {
        response.error(errors.getErrorString(error));
    })
})
